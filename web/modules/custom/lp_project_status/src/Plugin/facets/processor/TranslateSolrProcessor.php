<?php

namespace Drupal\lp_project_status\Plugin\facets\processor;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\TypedData\ComplexDataDefinitionInterface;
use Drupal\Core\TypedData\DataReferenceDefinitionInterface;
use Drupal\Core\TypedData\TranslatableInterface;
use Drupal\facets\Exception\InvalidProcessorException;
use Drupal\facets\FacetInterface;
use Drupal\facets\Processor\BuildProcessorInterface;
use Drupal\facets\Processor\ProcessorPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
//Connection Service
use Drupal\Core\Site\Settings;
use Solarium\Client;
use Solarium\Core\Client\Adapter\Curl;
use Symfony\Component\EventDispatcher\EventDispatcher;
//Project Organization
use Drupal\node\Entity\Node;

/**
 * Transforms the results to show the translated entity label.
 *
 * @FacetsProcessor(
 *   id = "translate_solr",
 *   label = @Translation("Transform Solr ID to labels"),
 *   description = @Translation("Get label from solr and transform Ids to label"),
 *   stages = {
 *     "build" = 5
 *   }
 * )
 */
class TranslateSolrProcessor extends ProcessorPluginBase implements BuildProcessorInterface, ContainerFactoryPluginInterface {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LanguageManagerInterface $language_manager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->languageManager = $language_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('language_manager'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet, array $results) {

    //$language_interface = $this->languageManager->getCurrentLanguage();

    /** @var \Drupal\Core\TypedData\DataDefinitionInterface $data_definition */
    /*
    $data_definition = $facet->getDataDefinition();

    $property = NULL;
    foreach ($data_definition->getPropertyDefinitions() as $k => $definition) {
      if ($definition instanceof DataReferenceDefinitionInterface) {
        $property = $k;
        break;
      }
    }

    if ($property === NULL) {
      throw new InvalidProcessorException("Field doesn't have an entity definition, so this processor doesn't work.");
    }

    $entity_type = $data_definition
      ->getPropertyDefinition($property)
      ->getTargetDefinition()
      ->getEntityTypeId();
      */
    /** @var \Drupal\facets\Result\ResultInterface $result */
    $ids = [];
    foreach ($results as $delta => $result) {
      $ids[$delta] = $result->getRawValue();
    }

    /*
    // Load all indexed entities of this type.
    $entities = $this->entityTypeManager
      ->getStorage($entity_type)
      ->loadMultiple($ids);
      */


// /////////////Create array of node ids of the organization////////////////

     $orginization_item_ids_arr = array();
    foreach ($results as $i => $result) {
      /*
      if (!isset($entities[$ids[$i]])) {
        unset($results[$i]);
        continue;
      }
      */
      $orginization_item_ids_arr[] = "item_id:" . $ids[$i];

    }
// /////////////Create string of node ids of the organization////////////////
   $orginization_item_ids_str = "";
   if(count($orginization_item_ids_arr)> 0){
      $orginization_item_ids_str = implode(" OR ", $orginization_item_ids_arr);
   }else{
     $orginization_item_ids_str = "item_id:1";
   }
      \Drupal::logger('my custom array with implodeee')->notice("Shaheen " . serialize($orginization_item_ids_str));


///Connecting solr
    try {
      $adapter = new Curl();
      $eventDispatcher = new EventDispatcher();
      $config = [
        'endpoint' => [
          'localhost' => [
            'host' => 'solr',
            'port' => '8983',
            'path' => '/',
            'core' => 'countries',
          ],
        ],
      ];
      $this->client = new Client($adapter, $eventDispatcher, $config);
    }
    catch (\Exception $e) {
      watchdog_exception('facets', $e);
    }  

//Get organizations record from solr d7 on the basis of node ids string
    $index = 'remote_organizations';
    $conditions = [];  
    $fields = []; 
    $sorts = []; 
    $num_rows = 1000;
    $items = [];

    try {
      $query = $this->client->createSelect();

      // Add the base conditions (index and language), then add custom ones.
      $query->createFilterQuery('index')->setQuery('index_id:' . $index);

      foreach ($conditions as $name => $value) {
        $query->createFilterQuery($name)->setQuery($name . ':' . $value);
      }


      //$item_ids = "item_id:5 OR item_id:4";
      $query->createFilterQuery('item_id')->setQuery($orginization_item_ids_str);

      // Set the projection of fields to retrieve.
      if ($fields) {
        $query->setFields($fields);
      }

      // The sorts are asc/desc.
      foreach ($sorts as $name => $value) {
        $query->addSort($name, $value);
      }

      // Limit the query.
      $query->setRows($num_rows);

      $resultsa = $this->client->select($query);
      //\Drupal::logger('facetsnewsa')->notice(serialize($resultsa));
      
      foreach ($resultsa as $document) {
        //\Drupal::logger('facetsnewsa_itema')->notice("iqbal is testing");
        $item = [];
        foreach ($document as $field => $value) {
          $item[$field] = $value;
        }
        $items[] = $item;
      }
      //\Drupal::logger('facetsnewsa_items')->notice(serialize($items));
    }
    catch (\Exception $e) {
      watchdog_exception('facetsnew', $e);
    }
    $res = '';
      foreach ($items as $key => $itemsss) {
         $res = $itemsss['ss_title_field'];

      }


//////////////my cutom end///////////




    // Loop over all results.
    foreach ($results as $i => $result) {
      /*
      if (!isset($entities[$ids[$i]])) {
        unset($results[$i]);
        continue;
      }
      */

      /** @var \Drupal\Core\Entity\ContentEntityBase $entity */
      /*
      $entity = $entities[$ids[$i]];

      // Check for a translation of the entity and load that instead if one's
      // found.
      if ($entity instanceof TranslatableInterface && $entity->hasTranslation($language_interface->getId())) {
        $entity = $entity->getTranslation($language_interface->getId());
      }

      */


//////////////Replace organization id with organization name code start ////////

//items array has the organization data from the solr on the basis of node ids
      $orgnization_name = "";
      foreach ($items as $key => $value) {
        // code...
        if($value['item_id'] == $ids[$i]) {
          $orgnization_name = $value['ss_title_field'];
          $orgnization_name = ucwords($orgnization_name);
        }
      }

      $results[$i]->setDisplayValue($orgnization_name);
      //$results[$i]->setDisplayValue($entity->label()); Actual code is this

//////////////Replace organization id with organization name code end ////////





    }

    // Return the results with the new display values.
    return $results;
  }

  // /**
  //  * {@inheritdoc}
  //  */
  // public function supportsFacet(FacetInterface $facet) {
  //   $data_definition = $facet->getDataDefinition();
  //   if ($data_definition->getDataType() === 'entity_reference') {
  //     return TRUE;
  //   }
  //   if (!($data_definition instanceof ComplexDataDefinitionInterface)) {
  //     return FALSE;
  //   }

  //   $property_definitions = $data_definition->getPropertyDefinitions();
  //   foreach ($property_definitions as $definition) {
  //     if ($definition instanceof DataReferenceDefinitionInterface) {
  //       return TRUE;
  //     }
  //   }
  //   return FALSE;
  // }

}
